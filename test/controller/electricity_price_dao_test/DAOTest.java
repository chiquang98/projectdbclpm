/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.electricity_price_dao_test;

import controller.ElectricityPriceDAO;
import java.util.ArrayList;
import model.ElectricityPrice;
import static org.testng.Assert.assertEquals;

/**
 *
 * @author ASUS
 */
abstract class DAOTest {
    
    protected ElectricityPriceDAO dao;
    protected ArrayList<ElectricityPrice> expectedResult;

    public DAOTest() {
        dao = new ElectricityPriceDAO();
        System.out.println("init expected result");// init expected result
        expectedResult = new ArrayList<ElectricityPrice>();
        try {
            expectedResult.add(new ElectricityPrice(0, 50, 1.678));
            expectedResult.add(new ElectricityPrice(51, 100, 1.734));
            expectedResult.add(new ElectricityPrice(101, 200, 2.014));
            expectedResult.add(new ElectricityPrice(201, 300, 2.536));
            expectedResult.add(new ElectricityPrice(301, 400, 2.834));
            expectedResult.add(new ElectricityPrice(401, 0, 2.927));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected void checkEqualsList(ArrayList<ElectricityPrice> result, ArrayList<ElectricityPrice> expResult) {
        assertEquals(result.size(), expResult.size());//test size
        for (int i = 0; i < result.size(); ++i) {
            //test attributes
            assertEquals(result.get(i).getLowerBound(), expResult.get(i).getLowerBound());
            assertEquals(result.get(i).getUpperBound(), expResult.get(i).getUpperBound());
            assertEquals(result.get(i).getPrice(), expResult.get(i).getPrice());
        }
    }
    
}
