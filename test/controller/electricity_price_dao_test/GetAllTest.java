/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.electricity_price_dao_test;

import java.util.ArrayList;
import model.ElectricityPrice;
import org.junit.Test;

/**
 *
 * @author ASUS
 */
public class GetAllTest extends DAOTest{
    /*
    Test phương thức getALL trong DAO
    */
    @Test
    public void test1(){
        System.out.println("getAll");
        ArrayList<ElectricityPrice> result = super.dao.getAll();
        super.checkEqualsList(result, super.expectedResult);
    }
}
