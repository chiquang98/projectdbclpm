/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.electricity_price_dao_test;

import controller.ElectricityPriceDAO;
import org.junit.Test;
import static org.testng.Assert.assertEquals;

/**
 *
 * @author ASUS
 */
public class DeleteAllTest extends DAOTest{
    /*
    Test phương thức DeleteALL trong DAO
    */
    @Test
    public void test1(){
        System.out.println("deleteAll");
        try {
            super.dao.conn.setAutoCommit(false);
            super.dao.deleteAll();
            int expSize = 0; // expected list size after delete
            int resultSize = super.dao.getAll().size();
             // get list size after delete
            //considered that getALL() method is correct
             //compare 2 sizes
            assertEquals(expSize, resultSize);
            //roll back database
            super.dao.conn.rollback();
            super.dao.conn.setAutoCommit(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
