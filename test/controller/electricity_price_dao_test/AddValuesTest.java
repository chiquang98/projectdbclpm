/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.electricity_price_dao_test;

import controller.ElectricityPriceDAO;
import java.util.ArrayList;
import model.ElectricityPrice;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author ASUS
 */
public class AddValuesTest extends DAOTest {
    /*
    Test phương thức addValues trong DAO
    */
    @Test
    public void test1(){
        System.out.println("addValues");
        ArrayList<ElectricityPrice> newPrices = new ArrayList<>();
        ElectricityPrice newPrice1 = new ElectricityPrice(501, 600, 2.990);
        ElectricityPrice newPrice2 = new ElectricityPrice(601, 700, 2.999);
        newPrices.add(newPrice1);
        newPrices.add(newPrice2);
        // add new prices to expeting result
        super.expectedResult.add(newPrice1);
        super.expectedResult.add(newPrice2);
        //
        
        try {
            super.dao.conn.setAutoCommit(false);
            super.dao.addValues(newPrices);
            ArrayList<ElectricityPrice> result = super.dao.getAll();
            //get result from database after adding new values
            // getAll is considered correct
            
            // compare result from database and expected result
            super.checkEqualsList(result, super.expectedResult);
            
            //rollback database
            super.dao.conn.rollback();
            super.dao.conn.setAutoCommit(true);
            //rollback expectedResult
            super.expectedResult.remove(super.expectedResult.size() -1);
            super.expectedResult.remove(super.expectedResult.size()-1);
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
