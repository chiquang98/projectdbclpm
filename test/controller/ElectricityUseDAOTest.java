/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Address;
import model.Customer;
import model.ElectricityUse;
import model.FullName;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class ElectricityUseDAOTest {
     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public ElectricityUseDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAll method, of class ElectricityUseDAO.
     */
//    @Test
//    public void testGetAll() {
//        System.out.println("getAll");
//        ElectricityUseDAO instance = new ElectricityUseDAO();
//        ArrayList<ElectricityUse> expResult = null;
//        ArrayList<ElectricityUse> result = instance.getAll();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getByMonth method, of class ElectricityUseDAO.
     */
    @Test
    public void testGetByMonth() {
         try {
             List<ElectricityUse> listUse = new ArrayList<ElectricityUse>();
              Customer babachCustomer = new Customer();
            babachCustomer.setCustomerID("001098021334");
            babachCustomer.setFullName(new FullName("Bách","Bá","Nguyễn"));
            babachCustomer.setDateOfBirth(sdf.parse("1998-12-30"));
            babachCustomer.setAddress(new Address("", "Gia Lâm","Hà Nội", "Việt Nam"));
            babachCustomer.setAccount(new Account("babach","123456"));
            babachCustomer.setEmail("ngbabach@gmail.com");
            babachCustomer.setTel("0333223535");
          

            Customer thanhdungCustomer = new Customer();
            thanhdungCustomer.setCustomerID("001188015634");
            thanhdungCustomer.setFullName(new FullName("Dung","Thanh","Nguyễn"));
            thanhdungCustomer.setDateOfBirth(sdf.parse("1998-04-04"));
            thanhdungCustomer.setAddress(new Address("", "Thanh Xuân","Hà Nội", "Việt Nam"));
            thanhdungCustomer.setAccount(new Account("thanhdung","123456"));
            thanhdungCustomer.setEmail("dungnt441998@gmail.com");
            thanhdungCustomer.setTel("0366189608");
        

            Customer chiquangCustomer = new Customer();
            chiquangCustomer.setCustomerID("013612379");
            chiquangCustomer.setFullName(new FullName("Quang","Chí","Trần"));
            chiquangCustomer.setDateOfBirth(sdf.parse("1998-04-01"));
            chiquangCustomer.setAddress(new Address("", "Đông Anh","Hà Nội", "Việt Nam"));
            chiquangCustomer.setAccount(new Account("chiquang","123456"));
            chiquangCustomer.setEmail("chiquang1498@gmail.com");
            chiquangCustomer.setTel("0975312798");
        

            Customer vanhieuCustomer = new Customer();
            vanhieuCustomer.setCustomerID("184288789");
            vanhieuCustomer.setFullName(new FullName("Hiệu","Văn","Nguyễn"));
            vanhieuCustomer.setDateOfBirth(sdf.parse("1998-07-21"));
            vanhieuCustomer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            vanhieuCustomer.setAccount(new Account("vanhieu","123456"));
            vanhieuCustomer.setEmail("vanhieunguyen221@gmail.com");
            vanhieuCustomer.setTel("0336838142");
         

             listUse.add(new ElectricityUse(37, babachCustomer,
                     sdf.parse("2019-05-01"), 0, 120));
             listUse.add(new ElectricityUse(38, thanhdungCustomer,
                     sdf.parse("2019-05-01"), 0, 243));
             listUse.add(new ElectricityUse(39, vanhieuCustomer,
                     sdf.parse("2019-05-01"), 0, 136));
             listUse.add(new ElectricityUse(40, chiquangCustomer,
                     sdf.parse("2019-05-01"), 0, 341));
             System.out.println("getByMonth");
             int month = 5;
             String year = "2019";
             ElectricityUseDAO instance = new ElectricityUseDAO();
             ArrayList<ElectricityUse> result = instance.getByMonth(month, year);
             assertEquals(listUse, result);
         } catch (ParseException ex) {
             Logger.getLogger(ElectricityUseDAOTest.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

}
