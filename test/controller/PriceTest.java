/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Customer;
import model.ElectricityUse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class PriceTest {

    public PriceTest() {
    }

    /**
     * Test of calculatePrice method, of class Price.
     */
//     ArrayList<ElectricityPrice> listPrice = priceDAO.getAll();
//        double totalPaid = 0;
//        int totalUse = user.getCurrentNumber() - user.getLastNumber();
//        for (int i = listPrice.size() - 1; i >= 0; i--) {
//            int tmp;
//            if (totalUse >= listPrice.get(i).getLowerBound()) {
//                tmp = totalUse - listPrice.get(i).getLowerBound();
//                if (listPrice.get(i).getLowerBound() != 0) {
//                    tmp += 1;
//                }
//                totalUse = listPrice.get(i).getLowerBound() - 1;
//                totalPaid += tmp * listPrice.get(i).getPrice();
//            }
//        }
//        return totalPaid;
//    }
    @Test
    public void testCalculatePrice() {
     
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 120, 240);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 20 * 2.014 + 50 * 1.734 + 50 * 1.678;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    // Start Test biên khoảng 0 - 50
    //số điện sử dụng -1 
    @Test
    public void testCalculatePrice1() {
      
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 120, 119);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 0;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    //số điện sử dụng 0 
    @Test
    public void testCalculatePrice2() {
     
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 120, 119);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 0;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 25 

    @Test
    public void testCalculatePrice3() {
    
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 120, 145);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 25 * 1.678;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 50

    @Test
    public void testCalculatePrice4() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 120, 170);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 50 * 1.678;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 51

    @Test
    public void testCalculatePrice5() {
     
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 120, 171);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 50 * 1.678 + 1 * 1.734;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    //số điện sử dụng 70
    @Test
    public void testCalculatePrice6() {
        
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 170);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 50 * 1.678 + 20 * 1.734;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 100

    @Test
    public void testCalculatePrice7() {
     
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 200);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double expResult = 50 * 1.678 + 50 * 1.734;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 101

    @Test
    public void testCalculatePrice8() {
 
        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 201);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 1 * 2.014;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 150

    @Test
    public void testCalculatePrice9() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 250);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 50 * 2.014;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 200

    @Test
    public void testCalculatePrice10() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 300);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 201

    @Test
    public void testCalculatePrice11() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 301);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 1 * 2.536;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 250

    @Test
    public void testCalculatePrice12() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 350);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 50 * 2.536;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 300

    @Test
    public void testCalculatePrice13() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 400);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 100 * 2.536;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 301

    @Test
    public void testCalculatePrice14() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 401);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 100 * 2.536 + 1 * 2.834;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 350

    @Test
    public void testCalculatePrice15() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 450);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 100 * 2.536 + 50 * 2.834;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng 400

    @Test
    public void testCalculatePrice16() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 500);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 100 * 2.536 + 100 * 2.834;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    //số điện sử dụng >400

    @Test
    public void testCalculatePrice17() {

        System.out.println("calculatePrice");
        ElectricityUse user = new ElectricityUse(0, null, null, 100, 550);
        ElectricityPriceDAO priceDAO = new ElectricityPriceDAO();
        Price instance = new Price();
        int totalUse = user.getCurrentNumber() - user.getLastNumber();//120
        double temp = 50 * 1.678 + 50 * 1.734 + 100 * 2.014 + 100 * 2.536 + 100 * 2.834 + 50
                * 2.927;
        double expResult = (double) Math.round((temp) * 1000) / 1000.0d;
        System.out.println(totalUse);
        double result = instance.calculatePrice(user, priceDAO);
        System.out.println(new Price().calculatePrice(user, priceDAO));
//        assertEquals(expResult, new Price().calculatePrice(user, priceDAO), 0.0);
        assertEquals(Math.round(expResult), Math.round(new Price().calculatePrice(user, priceDAO)), 0.0);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

}
