/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryan
 */
public class CustomerDAOTest {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    List<Customer> allCustomers;
    
    public CustomerDAOTest(){
        // Set up expected results
        allCustomers = new ArrayList<>();
        try{
            // Init customers list
            Customer babachCustomer = new Customer();
            babachCustomer.setCustomerID("001098021334");
            babachCustomer.setFullName(new FullName("Bách","Bá","Nguyễn"));
            babachCustomer.setDateOfBirth(sdf.parse("1998-12-30"));
            babachCustomer.setAddress(new Address("", "Gia Lâm","Hà Nội", "Việt Nam"));
            babachCustomer.setAccount(new Account("babach","123456"));
            babachCustomer.setEmail("ngbabach@gmail.com");
            babachCustomer.setTel("0333223535");
            allCustomers.add(babachCustomer);

            Customer thanhdungCustomer = new Customer();
            thanhdungCustomer.setCustomerID("001188015634");
            thanhdungCustomer.setFullName(new FullName("Dung","Thanh","Nguyễn"));
            thanhdungCustomer.setDateOfBirth(sdf.parse("1998-04-04"));
            thanhdungCustomer.setAddress(new Address("", "Thanh Xuân","Hà Nội", "Việt Nam"));
            thanhdungCustomer.setAccount(new Account("thanhdung","123456"));
            thanhdungCustomer.setEmail("dungnt441998@gmail.com");
            thanhdungCustomer.setTel("0366189608");
            allCustomers.add(thanhdungCustomer);

            Customer chiquangCustomer = new Customer();
            chiquangCustomer.setCustomerID("013612379");
            chiquangCustomer.setFullName(new FullName("Quang","Chí","Trần"));
            chiquangCustomer.setDateOfBirth(sdf.parse("1998-04-01"));
            chiquangCustomer.setAddress(new Address("", "Đông Anh","Hà Nội", "Việt Nam"));
            chiquangCustomer.setAccount(new Account("chiquang","123456"));
            chiquangCustomer.setEmail("chiquang1498@gmail.com");
            chiquangCustomer.setTel("0975312798");
            allCustomers.add(chiquangCustomer);

            Customer vanhieuCustomer = new Customer();
            vanhieuCustomer.setCustomerID("184288789");
            vanhieuCustomer.setFullName(new FullName("Hiệu","Văn","Nguyễn"));
            vanhieuCustomer.setDateOfBirth(sdf.parse("1998-07-21"));
            vanhieuCustomer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            vanhieuCustomer.setAccount(new Account("vanhieu","123456"));
            vanhieuCustomer.setEmail("vanhieunguyen221@gmail.com");
            vanhieuCustomer.setTel("0336838142");
            allCustomers.add(vanhieuCustomer);
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getListCustomer method, of class CustomerDAO.
     */
    @Test
    public void testGetListCustomer() {
        System.out.println("getListCustomer");
        CustomerDAO instance = new CustomerDAO();
        List<Customer> expResult = allCustomers;
        // Test
        List<Customer> result = instance.getListCustomer();
        assertEquals(expResult, result);
    }

    /**
     * Test of getByID method, of class CustomerDAO.
     */
    @Test
    public void testGetByID() {
        System.out.println("getByID");
        String cusID = "184288789";
        CustomerDAO instance = new CustomerDAO();
        
        Customer expResult = allCustomers.get(3);
        Customer result = instance.getByID(cusID);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertCustomer method, of class CustomerDAO.
     */
    @Test
    public void testInsertCustomer() {
        System.out.println("insertCustomer");
        Customer customer = null;
        try{
            customer = new Customer();
            customer.setCustomerID("1543543");
            customer.setFullName(new FullName("Hiệu","Văn","Nguyễn"));
            customer.setDateOfBirth(sdf.parse("1998-07-21"));
            customer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            customer.setAccount(new Account("vanhieu","123456"));
            customer.setEmail("vanhieunguyen221@gmail.com");
            customer.setTel("0336838142");
        }catch(ParseException e){
            e.printStackTrace();
        }
        CustomerDAO instance = new CustomerDAO();
        boolean expResult = true;
        boolean result = instance.insertCustomer(customer);
        assertEquals(expResult, result);
        
        // Check inserted customers
        Customer insertedCustomer = instance.getByID("1543543");
        assertEquals(customer, insertedCustomer);
        
        try{
            instance.conn.rollback();
            instance.conn.setAutoCommit(true);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    // Test insertCustomer method of class CustomerDAO
    // Test insertion if account is null
    @Test
    public void testInsertCustomerIfAccountNull(){
        System.out.println("insertCustomer");
        Customer customer = null;
        try{
            customer = new Customer();
            customer.setCustomerID("1543543");
            customer.setFullName(new FullName("Hiệu","Văn","Nguyễn"));
            customer.setDateOfBirth(sdf.parse("1998-07-21"));
            customer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            customer.setAccount(null);
            customer.setEmail("vanhieunguyen221@gmail.com");
            customer.setTel("0336838142");
        }catch(ParseException e){
            e.printStackTrace();
        }
        CustomerDAO instance = new CustomerDAO();
        boolean expResult = false;
        boolean result = instance.insertCustomer(customer);
        assertEquals(expResult, result);
        
        // Check inserted customers
        Customer insertedCustomer = instance.getByID("1543543");
        assertNull(insertedCustomer);
        
        try{
            instance.conn.rollback();
            instance.conn.setAutoCommit(true);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    // Test insertCustomer method of class CustomerDAO
    // Test insertion if address is null
    @Test
    public void testInsertCustomerIfAddressNull() {
        System.out.println("insertCustomer");
        Customer customer = null;
        try{
            customer = new Customer();
            customer.setCustomerID("1543543");
            customer.setFullName(new FullName("Hiệu","Văn","Nguyễn"));
            customer.setDateOfBirth(sdf.parse("1998-07-21"));
            customer.setAddress(null);
            customer.setAccount(new Account("vanhieu","123456"));
            customer.setEmail("vanhieunguyen221@gmail.com");
            customer.setTel("0336838142");
        }catch(ParseException e){
            e.printStackTrace();
        }
        CustomerDAO instance = new CustomerDAO();
        boolean expResult = false;
        boolean result = instance.insertCustomer(customer);
        assertEquals(expResult, result);
        
        // Check inserted customers
        Customer insertedCustomer = instance.getByID("1543543");
        assertNull(insertedCustomer);
        
        try{
            instance.conn.rollback();
            instance.conn.setAutoCommit(true);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    // Test insertCustomer method of class CustomerDAO
    // Test insertion if full name is null
    @Test
    public void testInsertCustomerIfFullNameNull() {
        System.out.println("insertCustomer");
        Customer customer = null;
        try{
            customer = new Customer();
            customer.setCustomerID("1543543");
            customer.setFullName(null);
            customer.setDateOfBirth(sdf.parse("1998-07-21"));
            customer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            customer.setAccount(new Account("vanhieu","123456"));
            customer.setEmail("vanhieunguyen221@gmail.com");
            customer.setTel("0336838142");
        }catch(ParseException e){
            e.printStackTrace();
        }
        CustomerDAO instance = new CustomerDAO();
        boolean expResult = false;
        boolean result = instance.insertCustomer(customer);
        assertEquals(expResult, result);
        
        // Check inserted customers
        Customer insertedCustomer = instance.getByID("1543543");
        assertNull(insertedCustomer);
        
        try{
            instance.conn.rollback();
            instance.conn.setAutoCommit(true);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Test of updateCustomer method, of class CustomerDAO
     */
    @Test
    public void testUpdateCustomer(){
        System.out.println("updateCustomer");
        Customer customer = null;
        try{
            customer = new Customer();
            customer.setCustomerID("184288789");
            customer.setFullName(new FullName("A","Văn","Trần"));
            customer.setDateOfBirth(sdf.parse("1998-05-11"));
            customer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            customer.setAccount(new Account("atran","123456"));
            customer.setEmail("avantran@gmail.com");
            customer.setTel("0324388585");
        }catch(ParseException e){
            e.printStackTrace();
        }
        
        CustomerDAO instance = new CustomerDAO();
        boolean expResult = true;
        boolean result = instance.updateCustomer(customer);
        assertEquals(expResult, result);
        
        // Check updated customer
        Customer updatedCustomer = instance.getByID(customer.getCustomerID());
        assertEquals(customer, updatedCustomer);
        
        try{
            instance.conn.rollback();
            instance.conn.setAutoCommit(true);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Test of deleteCustomer method, of class CustomerDAO
     */
    @Test
    public void testDeleteCustomer(){
        System.out.println("deleteCustomer");
        CustomerDAO instance = new CustomerDAO();
        
        // Insert a dummy customer
        Customer customer = null;
        try{
            customer = new Customer();
            customer.setCustomerID("1543543");
            customer.setFullName(new FullName("Hiệu","Văn","Nguyễn"));
            customer.setDateOfBirth(sdf.parse("1998-07-21"));
            customer.setAddress(new Address("", "Thanh Trì","Hà Nội", "Việt Nam"));
            customer.setAccount(new Account("vanhieu","123456"));
            customer.setEmail("vanhieunguyen221@gmail.com");
            customer.setTel("0336838142");
        }catch(ParseException e){
            e.printStackTrace();
        }
        // Check if insertion is successful
        boolean insertResult = instance.insertCustomer(customer);
        assertTrue(insertResult);
        
        String id = "1543543";
        
        boolean expResult = true;
        boolean result = instance.deleteCustomer(id);
        assertEquals(expResult, result);
        
        // Check if customer deleted
        Customer deletedCustomer = instance.getByID(id);
        assertNull(deletedCustomer);
        
        try{
            instance.conn.rollback();
            instance.conn.setAutoCommit(true);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
}
