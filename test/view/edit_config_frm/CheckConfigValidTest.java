/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.edit_config_frm;

import java.util.ArrayList;
import java.util.List;
import model.ElectricityPrice;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class CheckConfigValidTest extends EditConfigFrmTest {
    /*
     A. Bảng cấu hình giá được chấp nhận
     */

    @Test
    public void testA1() {
        ArrayList<ElectricityPrice> inputData = new ArrayList<ElectricityPrice>();
        inputData.add(new ElectricityPrice(0, 50, 1.678));
        inputData.add(new ElectricityPrice(51, 100, 1.734));
        inputData.add(new ElectricityPrice(101, 200, 2.014));
        inputData.add(new ElectricityPrice(201, 300, 2.536));
        inputData.add(new ElectricityPrice(301, 400, 2.836));
        inputData.add(new ElectricityPrice(401, -1, 2.927));
        String expected = null;// method return no error
        String actual = super.editConfigFrm.checkConfigValid(inputData);
        assertEquals(expected, actual);
    }
    /*
    B. (giới hạn dưới của cấu hình giá thứ i)!= (giới hạn trên của cầu hình giá thứ (i-1)) + 1
    */
    @Test
    public void testB1(){
        ArrayList<ElectricityPrice> inputData = new ArrayList<ElectricityPrice>();
        inputData.add(new ElectricityPrice(0, 50, 1.678));
        inputData.add(new ElectricityPrice(51, 100, 1.734));
        inputData.add(new ElectricityPrice(102, 200, 2.014));
        inputData.add(new ElectricityPrice(201, 300, 2.536));
        inputData.add(new ElectricityPrice(301, 400, 2.836));
        inputData.add(new ElectricityPrice(401, -1, 2.927));
        String expected = "Giới hạn dưới của dòng 3 phải hơn giới hạn trên của dòng 2 chính xác 1 đơn vị";// method return no error
        String actual = super.editConfigFrm.checkConfigValid(inputData);
        assertEquals(expected, actual);
    }
}
