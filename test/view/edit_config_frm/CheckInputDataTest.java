/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.edit_config_frm;

import model.ElectricityPrice;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author ASUS
 */
public class CheckInputDataTest extends EditConfigFrmTest{
    /*
    A. Input đúng
    */
    @Test
    public void testA1(){ // test input đúng cấu hình giá đầu tiên
        String[] objects = {"0","100","1.998"};
        ElectricityPrice expected = new ElectricityPrice(0, 100, 1.998);
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 0, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    @Test
    public void testA2(){ // test input đúng cấu hình giá cuối cùng
        String[] objects = {"200","--","1.998"};
        ElectricityPrice expected = new ElectricityPrice(200, -1, 1.998);
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 5, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    @Test
    public void testA3(){ // test input đúng các cấu hình ở giữa
        String[] objects = {"200","300","1.998"};
        ElectricityPrice expected = new ElectricityPrice(200, 300, 1.998);
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
                
    }    
    /*
    input sai
    */
    
    /*
    B.input có giá trị để chống
    */
    @Test 
    public void testB1(){ // để chống cột từ
        String[] objects = {"","300","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 1, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    @Test 
    public void testB2(){ // để chống cột Tới
        String[] objects = {"200","","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 1, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    @Test 
    public void testB3(){ // để chống cột Giá
        String[] objects = {"200","300",""};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 1, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    /*
    C. dữ liệu input có ký tự không phải là ký tự số
    */
    @Test 
    public void testC1(){ // Cột Từ có ký tự không phải là ký tự số
        String[] objects = {" 0 ","300","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 1, 6);
        super.checkEqualsPrice(expected, actual);
    }
    @Test 
    public void testC2(){ // Cột Tới có ký tự không phải là ký tự số
        String[] objects = {"200"," 300","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 1, 6);
        super.checkEqualsPrice(expected, actual);
    }
    @Test 
    public void testC3(){ // Cột Giá có ký tự không phải là ký tự số
        String[] objects = {"200","300","1.998lklk"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 1, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    /*
    D. cột từ cấu hình giá đầu tien khác 0
    */
    @Test 
    public void testD1(){
        String[] objects = {"10","300","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 0, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    /*
    E. cột tới cấu hình giá cuối cùng khác --
    */
    @Test 
    public void testE1(){
        String[] objects = {"500","600","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 5, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    /*
    F. kiểm tra số âm
    */
    @Test 
    public void testF1(){ // số âm ở cột từ
        String[] objects = {"-500","600","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    @Test 
    public void testF2(){// số âm ở cột tới
        String[] objects = {"500","-600","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    @Test 
    public void testF3(){ //số âm ở cột giá
        String[] objects = {"500","600","-1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    /*
    G.giá trị cột từ lớn hơn giá trị cột tới
    */
    @Test 
    public void testG1(){
        String[] objects = {"600","500","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    /*
    H. cột từ và tới có giá trị số nhưng ko là số nguyên
    */
    
    @Test
    public void testH1(){ //cột từ không phải là số nguyên
        String[] objects = {"200.5","500","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    @Test
    public void testH2(){ //cột tới không phải là số nguyên
        String[] objects = {"200","500.5","1.998"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    /*
    I. Giá trị cột giá (số thưc) có 2 dấu .
    */
    @Test
    public void testI1(){ //cột từ không phải là số nguyên
        String[] objects = {"200","500","1.99.8"};
        ElectricityPrice expected = null;
        ElectricityPrice actual = super.editConfigFrm.checkInputData(objects, 2, 6);
        super.checkEqualsPrice(expected, actual);
    }
    
    
    
    
    
    
    
    
}
