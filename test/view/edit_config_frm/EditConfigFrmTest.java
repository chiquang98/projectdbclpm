/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.edit_config_frm;

import model.ElectricityPrice;
import view.EditConfigFrm;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
abstract class EditConfigFrmTest {

    protected EditConfigFrm editConfigFrm;

    public EditConfigFrmTest() {
        editConfigFrm = new EditConfigFrm();
    }

    protected void checkEqualsPrice(ElectricityPrice expected, ElectricityPrice actual) {
        if (expected != null && actual != null) {
            assertEquals(expected.getLowerBound(), actual.getLowerBound());
            assertEquals(expected.getUpperBound(), actual.getUpperBound());
            assertEquals(expected.getPrice(), actual.getPrice(), 0);
        }
        else {
            assertEquals(expected, actual);
        }
    }

}
