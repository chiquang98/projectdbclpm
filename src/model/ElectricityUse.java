/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Ryan
 */
public class ElectricityUse {
    private int useID;
    private Customer customer;
    private Date useDate;
    private int lastNumber;
    private int currentNumber;

    public ElectricityUse() {
    }

    public ElectricityUse(int useID, Customer customer, Date useDate, int lastNumber, int currentNumber) {
        this.useID = useID;
        this.customer = customer;
        this.useDate = useDate;
        this.lastNumber = lastNumber;
        this.currentNumber = currentNumber;
    }

    public int getUseID() {
        return useID;
    }

    public void setUseID(int useID) {
        this.useID = useID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }

    public int getLastNumber() {
        return lastNumber;
    }

    public void setLastNumber(int lastNumber) {
        this.lastNumber = lastNumber;
    }

    public int getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(int currentNumber) {
        this.currentNumber = currentNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        
        if (!(obj instanceof ElectricityUse)) return false;
        
        ElectricityUse e = (ElectricityUse)obj;
        
        return (useID == e.useID && customer.equals(e.customer)
                && useDate.equals(e.useDate) && lastNumber == e.lastNumber
                && currentNumber == e.currentNumber);
    }
    
    
}
