/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class AccountSession {
    private AccountSession(){};
    private static final Account account = new Account("User1", "12345"); 
    //get instance
    public static AccountSession getInstance(){
        return new AccountSession();
    }
    // get logged in account
    public static Account getAccount(){
        return account;
    } 
    // write to log file (log.txt)
    public static void writeLog(String action){
        String content="";
        //get system time
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        //add system time
        content += simpleDateFormat.format(calendar.getTime()) + " | ";
        // add account username
        content += account.getUsername() + " | ";
        // add aciton
        content += action + "\n";
        //get file log.txt
        File logFile = new File("src\\log.txt");
        if (logFile.exists()){
            try {
                // log file is found
                //output to file
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(logFile,true));
                bufferedWriter.append(content);
                bufferedWriter.close();
                System.out.println(content);
            } catch (IOException ex) {
              //already solved
            }       
        }
        else {
            //log file is not found
            JOptionPane.showMessageDialog(null, "Log file not found");
            System.exit(0);
        }
    }
    
}
