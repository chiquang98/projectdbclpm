/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ryan
 */
public class FullName {
    private String firstName;
    private String middleName;
    private String lastName;

    public FullName() {
    }

    public FullName(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    @Override
    public String toString(){
        return lastName + " " + middleName + " " + firstName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) { 
            return true; 
        }
        
        if (!(obj instanceof FullName)){
            return false;
        }
        
        FullName f = (FullName)obj;
        
        return (firstName.equals(f.getFirstName()) 
                && middleName.equals(f.getMiddleName()) 
                && lastName.equals(f.getLastName()));
    }
    
    
}
