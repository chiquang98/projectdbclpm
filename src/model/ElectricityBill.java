/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Ryan
 */
public class ElectricityBill {
    private int billID;
    private ElectricityUse electricityUse;
    private Date payDate;
    private ArrayList<ElectricityPrice> priceList;
    private double totalPaid;

    public ElectricityBill() {
    }

    public ElectricityBill(int billID, ElectricityUse electricityUse, Date payDate, ArrayList<ElectricityPrice> priceList, double totalPaid) {
        this.billID = billID;
        this.electricityUse = electricityUse;
        this.payDate = payDate;
        this.priceList = priceList;
        this.totalPaid = totalPaid;
    }

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    public ElectricityUse getElectricityUse() {
        return electricityUse;
    }

    public void setElectricityUse(ElectricityUse electricityUse) {
        this.electricityUse = electricityUse;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public ArrayList<ElectricityPrice> getPriceList() {
        return priceList;
    }

    public void setPriceList(ArrayList<ElectricityPrice> priceList) {
        this.priceList = priceList;
    }

    public double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(double totalPaid) {
        this.totalPaid = totalPaid;
    }
    
}
