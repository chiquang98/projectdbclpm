/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ryan
 */
public class ElectricityPrice {
    private int lowerBound;
    private int upperBound;
    private double price;

    public ElectricityPrice() {
    }

    public ElectricityPrice(int lowerBound, int upperBound, double price) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.price = price;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public int getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
}
