/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ryan
 */
public class Address {
    private String addressLine;
    private String district;
    private String city;
    private String country;

    public Address() {
    }

    public Address(String addressLine, String district, String city, String country) {
        this.addressLine = addressLine;
        this.district = district;
        this.city = city;
        this.country = country;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    @Override
    public String toString(){
        return addressLine + ", " + district + ", " + city + ", " + country;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) { 
            return true; 
        }
        
        if (!(obj instanceof Address)){
            return false;
        }
        
        Address a = (Address)obj;
        
        return (addressLine.equals(a.addressLine) && district.equals(a.getDistrict()) 
                && city.equals(a.getCity()) && country.equals(a.getCountry()));
    }
    
}
