/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.Address;
import model.Customer;
import model.FullName;

/**
 * Class for accessing and altering Customer data in the database
 * Singleton class
 */
public class CustomerDAO {

    public Connection conn;

    public CustomerDAO() {
        try {
            conn = DAO.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets all Customers from database
     * 
     * @return List of all customers, empty if there is no customer
     */
    public List<Customer> getListCustomer() {
        List<Customer> listCustomer = new ArrayList<>();
        try {
            String sql = "SELECT * FROM customer, fullname, account, address WHERE "
                    + "customer.fullnameId = fullname.id "
                    + "AND customer.accountId = account.id "
                    + "AND customer.addressId = address.id";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Customer customer = new Customer();
                customer.setCustomerID(rs.getString("customerID"));
                customer.setFullName(new FullName(rs.getString("firstName"), rs.getString("middleName"), rs.getString("lastName")));
                customer.setDateOfBirth(rs.getDate("dateOfBirth"));
                customer.setAddress(new Address(rs.getString("addressLine"), rs.getString("district"), rs.getString("city"), rs.getString("country")));
                customer.setAccount(new Account(rs.getString("username"), rs.getString("password")));
                customer.setEmail(rs.getString("email"));
                customer.setTel(rs.getString("tel"));
                listCustomer.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCustomer;
    }

    /**
     * Get a Customer by id
     * 
     * @param cusID id of the customer
     * @return Customer with the id, null if there is no customer with the id
     */
    public Customer getByID(String cusID) {
        Customer customer = null;
        try {
            String sql = "SELECT * FROM customer, fullname, account, address WHERE "
                    + "customer.fullnameId = fullname.id "
                    + "AND customer.accountId = account.id "
                    + "AND customer.addressId = address.id "
                    + "AND customer.customerID = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, cusID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                customer = new Customer();
                customer.setCustomerID(rs.getString("customerID"));
                customer.setFullName(new FullName(rs.getString("firstName"), rs.getString("middleName"), rs.getString("lastName")));
                customer.setDateOfBirth(rs.getDate("dateOfBirth"));
                customer.setAddress(new Address(rs.getString("addressLine"), rs.getString("district"), rs.getString("city"), rs.getString("country")));
                customer.setAccount(new Account(rs.getString("username"), rs.getString("password")));
                customer.setEmail(rs.getString("email"));
                customer.setTel(rs.getString("tel"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customer;
    }
    
    /**
     * Insert a Customer into database
     * 
     * @param customer Customer to be inserted
     * @return true if inserted, false if failed;
     */
    public boolean insertCustomer(Customer customer){
        if (customer.getAccount() == null || customer.getAddress() == null || customer.getFullName() == null){
            return false;
        }
        String insertAccountSql = "INSERT INTO account(username, password) VALUES(?, ?)";
        String insertAddressSql = "INSERT INTO address(addressLine, district, city, country) VALUES(?, ?, ?, ?)";
        String insertFullNameSql = "INSERT INTO fullname(firstName, middleName, lastName) VALUES(?, ?, ?)";
        String insertCustomerSql = "INSERT INTO customer(customerID, dateOfBirth, email, tel, accountID, addressID, fullnameID) VALUES(?, ?, ?, ?, ?, ?, ?)";
        try{
            conn.setAutoCommit(false);
            
            // Insert account
            PreparedStatement ps = conn.prepareStatement(insertAccountSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, customer.getAccount().getUsername());
            ps.setString(2, customer.getAccount().getPassword());
            // If not inserted into account table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            int accountId = rs.getInt(1);
            
            // Insert address
            ps = conn.prepareStatement(insertAddressSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, customer.getAddress().getAddressLine());
            ps.setString(2, customer.getAddress().getDistrict());
            ps.setString(3, customer.getAddress().getCity());
            ps.setString(4, customer.getAddress().getCountry());
            // If not inserted into address table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            rs = ps.getGeneratedKeys();
            rs.next();
            int addressId = rs.getInt(1);
            
            // Insert full name
            ps = conn.prepareStatement(insertFullNameSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, customer.getFullName().getFirstName());
            ps.setString(2, customer.getFullName().getMiddleName());
            ps.setString(3, customer.getFullName().getLastName());
            // If not inserted into fullname table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            rs = ps.getGeneratedKeys();
            rs.next();
            int fullNameId = rs.getInt(1);
            
            // Insert customer
            ps = conn.prepareStatement(insertCustomerSql);
            ps.setString(1, customer.getCustomerID());
            ps.setDate(2, new Date(customer.getDateOfBirth().getTime()));
            ps.setString(3, customer.getEmail());
            ps.setString(4, customer.getTel());
            ps.setInt(5, accountId);
            ps.setInt(6, addressId);
            ps.setInt(7, fullNameId);
            // If not inserted into customer table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // If all inserted successfully
            //conn.commit();
            //conn.setAutoCommit(true);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    /**
    * Update a customer
    * 
    * @param customer Customer to be updated
    * @return true if updated, false if failed
    **/
    public boolean updateCustomer(Customer customer){
        String getIdSql = "SELECT accountId, addressId, fullNameId FROM customer WHERE customerID = ?";
        String updateAccountSql = "UPDATE account SET username = ?, password = ? WHERE id = ?";
        String updateAddressSql = "UPDATE address SET addressLine = ?, district = ?, city = ?, country = ? WHERE id = ?";
        String updateFullNameSql = "UPDATE fullname SET firstName = ?, middleName = ?, lastName = ? WHERE id = ?";
        String updateCustomerSql = "UPDATE customer SET dateOfBirth = ?, email = ?, tel = ? WHERE customerID = ?";
        try{
            conn.setAutoCommit(false);
            
            // Get ids of account, address, fullname for the customer
            PreparedStatement ps = conn.prepareStatement(getIdSql);
            ps.setString(1, customer.getCustomerID());
            ResultSet rs = ps.executeQuery();
            int accountId, fullNameId, addressId;
            if (rs.next()){
                accountId = rs.getInt("accountId");
                addressId = rs.getInt("addressId");
                fullNameId = rs.getInt("fullNameId");
            } else {
                conn.rollback();
                return false;
            }
            
            // Update account
            ps = conn.prepareStatement(updateAccountSql);
            ps.setString(1, customer.getAccount().getUsername());
            ps.setString(2, customer.getAccount().getPassword());
            ps.setInt(3, accountId);
            // If update account table failed
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // Update address
            ps = conn.prepareStatement(updateAddressSql);
            ps.setString(1, customer.getAddress().getAddressLine());
            ps.setString(2, customer.getAddress().getDistrict());
            ps.setString(3, customer.getAddress().getCity());
            ps.setString(4, customer.getAddress().getCountry());
            ps.setInt(5, addressId);
            // If update address table failed
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // Update full name
            ps = conn.prepareStatement(updateFullNameSql);
            ps.setString(1, customer.getFullName().getFirstName());
            ps.setString(2, customer.getFullName().getMiddleName());
            ps.setString(3, customer.getFullName().getLastName());
            ps.setInt(4, fullNameId);
            // If update fullname table failed
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // Update customer
            ps = conn.prepareStatement(updateCustomerSql);
            ps.setDate(1, new Date(customer.getDateOfBirth().getTime()));
            ps.setString(2, customer.getEmail());
            ps.setString(3, customer.getTel());
            ps.setString(4, customer.getCustomerID());
            // If update customer table failed
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            //conn.commit();
            //conn.setAutoCommit(true);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Delete a customer from database
     * 
     * @param id id of the customer
     * @return true if deleted, false if failed to delete
     */
    public boolean deleteCustomer(String id){
        String getIdSql = "SELECT accountId, addressId, fullNameId FROM customer WHERE customerID = ?";
        String deleteCustomerSql = "DELETE FROM customer WHERE customerID = ?";
        String deleteAccountSql = "DELETE FROM account WHERE id = ?";
        String deleteAddressSql = "DELETE FROM address WHERE id = ?";
        String deleteFullNameSql = "DELETE FROM fullname WHERE id = ?";
        try{
            conn.setAutoCommit(false);
            // Get ids of account, address, fullname for the customer
            PreparedStatement ps = conn.prepareStatement(getIdSql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            int accountId, fullNameId, addressId;
            if (rs.next()){
                accountId = rs.getInt("accountId");
                addressId = rs.getInt("addressId");
                fullNameId = rs.getInt("fullNameId");
            } else {
                conn.rollback();
                return false;
            }
            
            // Delete customer
            ps = conn.prepareStatement(deleteCustomerSql);
            ps.setString(1, id);
            // If fail to delete from customer table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // Delete account
            ps = conn.prepareStatement(deleteAccountSql);
            ps.setInt(1, accountId);
            // If failed to delete from account table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // Delete address
            ps = conn.prepareStatement(deleteAddressSql);
            ps.setInt(1, addressId);
            // If failed to delete from address table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            // Delete full name
            ps = conn.prepareStatement(deleteFullNameSql);
            ps.setInt(1, fullNameId);
            // If failed to delete from fullname table
            if (ps.executeUpdate() == 0){
                conn.rollback();
                return false;
            }
            
            //conn.commit();
            //conn.setAutoCommit(true);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
