/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.ElectricityUse;

/**
 *
 * @author Admin
 */
public class Statistic {

    public int thongKeLuongDienDungNhieuNhatTrongThang(ArrayList<ElectricityUse> listUse) {
        int threshold = 100;
        int max = 0;
        for (ElectricityUse use : listUse) {
            int numberUsingByMonth = use.getCurrentNumber() - use.getLastNumber();
            if (numberUsingByMonth > threshold) {
                if (numberUsingByMonth >= max) {
                    max = numberUsingByMonth;
                }
            }
        }
        return max;

    }

    public int thongKeLuongDienDungItNhatTrongThang(ArrayList<ElectricityUse> listUse) {
        int threshold = 100;
        int max = listUse.get(0).getCurrentNumber() - listUse.get(0).getLastNumber();
        for (ElectricityUse use : listUse) {
            int numberUsingByMonth = use.getCurrentNumber() - use.getLastNumber();
            if (numberUsingByMonth > threshold) {
                if (numberUsingByMonth < max) {
                    max = numberUsingByMonth;
                }
            }
        }
        return max;

    }

}
