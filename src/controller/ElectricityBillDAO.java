/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.ElectricityBill;
import model.ElectricityUse;

/**
 *
 * @author chiqu
 */
public class ElectricityBillDAO {

    Connection conn;
    //tạo connection với db
    public ElectricityBillDAO() {
        try {
            conn = DAO.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Lấy hóa đơn theo UseID
    public ElectricityBill getBillbyUseID(int useID) {
        ElectricityBill bill = null;
        try {
            System.out.println(useID);
            String sql = "SELECT * FROM electricitybill where electricityUseID = ?";
            ElectricityUse electricityUse = new ElectricityUse();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, useID);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                bill = new ElectricityBill();
                bill.setBillID(rs.getInt("billID"));
                System.out.println(bill.getBillID());
                electricityUse.setUseID(rs.getInt("electricityUseID"));
                bill.setElectricityUse(electricityUse);
                bill.setPayDate(rs.getDate("payDate"));
                bill.setTotalPaid(rs.getDouble("totalPaid"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ElectricityBillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bill;
    }
}
