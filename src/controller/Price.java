/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.ElectricityPrice;
import model.ElectricityUse;

/**
 *
 * @author Admin
 */
public class Price {
        public  double calculatePrice(ElectricityUse user,ElectricityPriceDAO priceDAO) {
        ArrayList<ElectricityPrice> listPrice = priceDAO.getAll();
        double totalPaid = 0;
        int totalUse = user.getCurrentNumber() - user.getLastNumber();
        for (int i = listPrice.size() - 1; i >= 0; i--) {
            int tmp;
            if (totalUse >= listPrice.get(i).getLowerBound()) {
                tmp = totalUse - listPrice.get(i).getLowerBound();
                if (listPrice.get(i).getLowerBound() != 0) {
                    tmp += 1;
                }
                totalUse = listPrice.get(i).getLowerBound() - 1;
                totalPaid += tmp * listPrice.get(i).getPrice();
            }
        }
        return totalPaid;
    }
}
