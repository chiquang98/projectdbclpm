/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.ElectricityUse;

/**
 *
 * @author chiqu
 */
public class ElectricityUseDAO {

    Connection conn;
    //tạo connection với db
    public ElectricityUseDAO() {
        try {
            conn = DAO.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(ElectricityUseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Lấy hết danh sách sử dụng
    public ArrayList<ElectricityUse> getAll() {
        String sql = "SELECT useID customerID, useDate, lastNumber, currentNumber FROM electricityuse";
        ArrayList<ElectricityUse> eUses = new ArrayList<ElectricityUse>();
        Customer temp = new Customer();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ElectricityUse eUse = new ElectricityUse();
                eUse.setUseID(rs.getInt("useID"));
                temp.setCustomerID(rs.getString("customerID"));
                eUse.setCustomer(temp);
                eUse.setUseDate(rs.getDate("useDate"));
                eUse.setLastNumber(rs.getInt("lastNumber"));
                eUse.setCurrentNumber(rs.getInt("currentNumber"));
                eUses.add(eUse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ElectricityUse.getAll() = " + eUses.size());
        return eUses;
    }
    //Lấy danh sách sử dụng theo tháng và năm
    public ArrayList<ElectricityUse> getByMonth(int month, String year) {
        String sql = "SELECT useID, customerID, useDate, lastNumber, currentNumber FROM electricityuse where MONTH(useDate) = ? and YEAR(useDate) = ?";
        CustomerDAO customerDAO = new CustomerDAO();
        ArrayList<ElectricityUse> eUses = new ArrayList<ElectricityUse>();
        Customer temp = new Customer();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, month);
            ps.setString(2, year);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ElectricityUse eUse = new ElectricityUse();
                eUse.setUseID(rs.getInt("useID"));
                temp = customerDAO.getByID(rs.getString("customerID"));
                System.out.println(temp.getFullName().toString());
                eUse.setCustomer(temp);
                eUse.setUseDate(rs.getDate("useDate"));
                eUse.setLastNumber(rs.getInt("lastNumber"));
                eUse.setCurrentNumber(rs.getInt("currentNumber"));
                eUses.add(eUse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ElectricityUse.getByMonth = " + eUses.size());
        return eUses;
    }

}
