/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ElectricityPrice;

/**
 *
 * @author ASUS
 */
public class ElectricityPriceDAO {

    public Connection conn;

    public ElectricityPriceDAO() {
        try {
            conn = DAO.getInstance().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(ElectricityPriceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Lấy hết danh sách giá
    public ArrayList<ElectricityPrice> getAll() {
        String sql = "SELECT lowerBound, upperBound, price FROM electricityprice ORDER BY lowerBound ASC, upperBound ASC";
        ArrayList<ElectricityPrice> ePrices = new ArrayList<ElectricityPrice>();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ElectricityPrice ePrice = new ElectricityPrice();
                ePrice.setLowerBound(rs.getInt("lowerBound"));
                ePrice.setUpperBound(rs.getInt("UpperBound"));
                ePrice.setPrice(rs.getDouble("price"));
                ePrices.add(ePrice);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ElectricityPriceDAO.getAll() = " + ePrices.size());
        return ePrices;
    }
  
    /**
     * Delete all prices from database
     */
    public void deleteAll() {
        String sql = "DELETE FROM electricityprice";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            int result = ps.executeUpdate();
            System.out.println("ElectricityPriceDAO.deleteAll() = " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a list of electricity prices into the database
     * 
     * @param ePrices List of prices to be added
     */
    public void addValues(ArrayList<ElectricityPrice> ePrices) {
        String sql = "INSERT INTO electricityprice(lowerBound, upperBound, price) VALUES ";
        for (int i = 0; i < ePrices.size(); i++) {
            String value = "(" + ePrices.get(i).getLowerBound() + ", "
                    + ePrices.get(i).getUpperBound() + ", "
                    + ePrices.get(i).getPrice() + "), ";
            sql += value;
        }
        sql = sql.substring(0, sql.length() - 2);
        sql += ";";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            int result = ps.executeUpdate();
            System.out.println("ElectricityPriceDAO.addValues = " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
