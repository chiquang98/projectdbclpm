/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.sendEmail;

import controller.CustomerDAO;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import model.*;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

/**
 * Frame for creating email and sending it to customers 
 * @author Ryan
 */
public class SendEmailFrm extends javax.swing.JFrame implements ActionListener {

    private List<Customer> listCustomer;
    private ArrayList<JCheckBox> listCheckBox;
    private ArrayList<String> listReceiver;
    private CustomerDAO customerDAO;

    /**
     * Creates new form SendEmailFrm
     */
    public SendEmailFrm() {
        initComponents();
        this.setLocationRelativeTo(this);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        // Initiate lists
        listCustomer = new ArrayList<>();
        listCheckBox = new ArrayList<>();
        listReceiver = new ArrayList<>();
        
        // Initiate controller object
        customerDAO = new CustomerDAO();

        // Set default button
        JRootPane rootPane = SwingUtilities.getRootPane(btnSend); 
        rootPane.setDefaultButton(btnSend);
        
        // Request focus on subject text field
        txtSubject.requestFocusInWindow();
        
        // Set up glasspane comsume mouse input
        getGlassPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                e.consume();
            }
        });
        
        //Init list customer
        listCustomer= customerDAO.getListCustomer();
        for (Customer c:listCustomer){
            listCheckBox.add(new JCheckBox());
        }

        initReceiverTable();
        btnSend.addActionListener(this);
    }

    /*
    * Overrides action performed for buttons
    */
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton btn = (JButton) e.getSource();
        // Determines which button is clicked
        // If clicked button is Send button
        if (btn.equals(btnSend)) {
            //Get list of receiver
            for (int i = 0; i < listCustomer.size(); i++) {
                if (tblCustomer.getValueAt(i, 0).equals(true)) {
                    listReceiver.add(listCustomer.get(i).getEmail());
                }
            }
            // Check if there is no receivers
            if (listReceiver.isEmpty()) {
                JOptionPane.showConfirmDialog(this, "Please select at least one receiver", "No receiver selected", JOptionPane.CLOSED_OPTION);
            }
            // Check if subject is empty
            else if (txtSubject.getText().trim().isEmpty()) {
                JOptionPane.showConfirmDialog(this, "Please enter subject", "No subject", JOptionPane.CLOSED_OPTION);
            }
            // Check if content is empty
            else if (txtContent.getText().trim().isEmpty()) {
                JOptionPane.showConfirmDialog(this, "Please enter content", "No content", JOptionPane.CLOSED_OPTION);
            }
            // If everything's validated
            else {
                //Get username and password of gmail account from user
                EnterEmailAccountFrm enterEmailAccountFrm = new EnterEmailAccountFrm(this);
                enterEmailAccountFrm.setVisible(true);
            }
        }
    }

    /*
    * Initiate receivers table's look
    */
    private void initReceiverTable() {
        tblCustomer.setModel(new CustomerTableModel());
        tblCustomer.getTableHeader().setUI(null);
        tblCustomer.addMouseListener(new JTableMouseListener(tblCustomer));

        //Personalize
        tblCustomer.getColumnModel().getColumn(0).setMaxWidth(30);
        tblCustomer.getColumnModel().getColumn(0).setPreferredWidth(30);

        //Disable selection and edit in customer table
        tblCustomer.setFocusable(false);
        tblCustomer.setRowSelectionAllowed(false);

    }

    /*
    * Send an email to customers through account whose email and password are entered from keyboard
    * @param {String} email - Email address of sender
    * @param {String} password - Password of sender
    */
    public void sendEmail(String email, String password) {
        SendEmailThread sendEmailThread = new SendEmailThread(email, password, this);
        sendEmailThread.start();
    }
    
    /*
    * Set current status of frame based on background process
    * @param {String} txt - Message to be displayed
    * @param {boolean} waiting - Is the frame waiting for a background process to be done
    */
    public void setStatus(String txt, boolean waiting){
        txtStatus.setText(txt);
        if (waiting){
            getGlassPane().setVisible(true);
            getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
        else{
            getGlassPane().setVisible(false);
            getGlassPane().setCursor(Cursor.getDefaultCursor());
        }
    }

    /*
    * Background thread for sending emails
    */
    class SendEmailThread extends Thread {
        String email;
        String password;
        SendEmailFrm sendEmailFrm;

        /*
        * Initiate thread
        * @param {String} email - Email address of sender
        * @param {String} password - Password of sender
        * @param {SendEmailFrm} sendEmailFrm - Parent frame of the thread
        */
        public SendEmailThread(String email, String password, SendEmailFrm sendEmailFrm) {
            this.email = email;
            this.password = password;
            this.sendEmailFrm = sendEmailFrm;
        }

        @Override
        public void run() {
            //Set up sender email
            String from = email;
            //Set up receiver email
            String to = listReceiver.get(0);
            if (listReceiver.size() > 1) {
                for (int i = 1; i < listReceiver.size(); i++) {
                    to = to + ", " + listReceiver.get(i);
                }
            }
            //Set up host (gmail)
            String host = "smtp.gmail.com";
            //Get system properties
            Properties properties = System.getProperties();
            //Set up mail server
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.post", "465");
            properties.put("mail.smtp.ssl.enable", "true");
            properties.put("mail.smtp.auth", "true");
            //Get the session username and password
            Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(email, password);
                }
            });
            session.setDebug(true);
            try {
                //Create default MimeMessage instance
                MimeMessage message = new MimeMessage(session);
                //Set from, to
                message.setFrom(new InternetAddress(from));
                message.addRecipients(Message.RecipientType.TO, to);
                //Set subject
                message.setSubject(txtSubject.getText());
                //Set content
                message.setText(txtContent.getText());
                //Send
                System.out.println("Sending...");
                sendEmailFrm.setStatus("Sending...", true);
                Transport.send(message);
                System.out.println("Message send successfully");
                sendEmailFrm.setStatus("Message send successfully", false);
            } catch (MessagingException mex) {
                mex.printStackTrace();
                sendEmailFrm.setStatus("Failed to send message", false);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblCustomer = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtContent = new javax.swing.JTextArea();
        txtSubject = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnSend = new javax.swing.JButton();
        txtStatus = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tblCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCustomer);

        txtContent.setColumns(20);
        txtContent.setRows(5);
        jScrollPane2.setViewportView(txtContent);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel1.setText("Tiêu đề:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setText("Nội dung:");

        btnSend.setText("Gửi");

        txtStatus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtStatus.setToolTipText("");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel4.setText("Người nhận:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 662, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSend, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 435, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSend))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 573, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /*
    * Table model for customer table
    */
    class CustomerTableModel extends DefaultTableModel {

        private String[] columnNames = {"Select", "Customer"};
        private final Class<?>[] columnTypes = new Class<?>[]{Boolean.class, String.class};

        @Override
        public int getColumnCount() {
            //return columnNames.length;
            return 2;
        }

        @Override
        public Object getValueAt(int row, int column) {
            switch (column) {
                case 0:
                    return listCheckBox.get(row).isSelected();
                case 1:
                    return listCustomer.get(row).getFullName().toString();
                default:
                    return null;
            }
        }

        @Override
        public void setValueAt(Object aValue, int row, int column) {
            if (column == 0) {
                listCheckBox.get(row).setSelected((Boolean) aValue);
            }
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public int getRowCount() {
            return listCustomer.size();
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return columnTypes[columnIndex];
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }

    }

    /*
    * Listener for table model
    */
    class JTableMouseListener extends MouseAdapter {

        private final JTable table;

        public JTableMouseListener(JTable table) {
            this.table = table;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int column = table.getColumnModel().getColumnIndexAtX(e.getX());
            int row = e.getY() / table.getRowHeight();
            if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
                Boolean value = (Boolean) table.getValueAt(row, 0);
                tblCustomer.setValueAt(!value, row, 0);
                ((DefaultTableModel) tblCustomer.getModel()).fireTableDataChanged();
            }
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSend;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCustomer;
    private javax.swing.JTextArea txtContent;
    private javax.swing.JLabel txtStatus;
    private javax.swing.JTextField txtSubject;
    // End of variables declaration//GEN-END:variables
}
