package view;

import controller.ElectricityPriceDAO;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import model.AccountSession;
import model.ElectricityPrice;

public class EditConfigFrm extends JFrame implements ActionListener{

    private JPanel contentPane;
    private JButton btnConfirm;
    private JButton btnCancel;
    private JScrollPane scrollPane;
    private JTable tblPrice;
    private ArrayList<ElectricityPrice> ePrices;
    private ElectricityPriceDAO electricityPriceDAO;
    private JButton btnAdd;
    private JButton btnDelete;
    private OnDataChangeListener onDataChangeListener;

    //constructor
    public EditConfigFrm(ArrayList<ElectricityPrice> ePrices, ElectricityPriceDAO electricityPriceDAO) {
        initComponent();
        this.ePrices = ePrices;
        fillTable(this.ePrices);
        this.electricityPriceDAO = electricityPriceDAO;
    }

    public EditConfigFrm() {
    }

    //setter
    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        this.onDataChangeListener = onDataChangeListener;
    }

    public void setePrices(ArrayList<ElectricityPrice> ePrices) {
        this.ePrices = ePrices;
        fillTable(this.ePrices);
    }

    /**
     * Create the frame.
     */
    private void initComponent() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Sửa cấu hình");
        setBounds(100, 100, 582, 428);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(null);
        setContentPane(contentPane);

        Font labelFont = new Font("Arial", Font.BOLD, 24);
        Font buttonFont = new Font("Arial", Font.BOLD, 20);
        Font tableCellFont = new Font("Arial", Font.PLAIN, 17);
        Font tableHeaderFont = new Font("Arial", Font.BOLD, 17);

        btnConfirm = new JButton("Xác nhận");
        btnConfirm.setBounds(275, 316, 130, 40);
        btnConfirm.setFont(buttonFont);
        contentPane.add(btnConfirm);

        btnCancel = new JButton("Hủy");
        btnCancel.setBounds(420, 316, 125, 40);
        btnCancel.setFont(buttonFont);
        contentPane.add(btnCancel);

        btnAdd = new JButton("Thêm");
        btnAdd.setBounds(15, 316, 115, 40);
        btnAdd.setFont(buttonFont);
        contentPane.add(btnAdd);

        btnDelete = new JButton("Xóa");
        btnDelete.setBounds(145, 316, 115, 40);
        btnDelete.setFont(buttonFont);
        contentPane.add(btnDelete);

        scrollPane = new JScrollPane();
        scrollPane.setBounds(15, 16, 530, 288);
        contentPane.add(scrollPane);

        String[] cols = {"STT", "Từ (kWh)", "Tới (kWh)", "Giá (kVND/kWh)"};

        DefaultTableModel dtm = new DefaultTableModel(cols, 8) {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {// cấm sửa dự liệu cột thứ 0 = cột STT
                    return false;
                }
                return true;
            }
        };

        tblPrice = new JTable(dtm);
        tblPrice.setRowHeight(30);
        tblPrice.getTableHeader().setFont(tableHeaderFont);
        tblPrice.getTableHeader().setReorderingAllowed(false);
        tblPrice.setFont(tableCellFont);
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.RIGHT);

        TableColumnModel tcm = tblPrice.getColumnModel();
        tcm.getColumn(0).setPreferredWidth(10);
        for (int i = 0; i < tblPrice.getColumnCount(); i++) {
            tcm.getColumn(i).setResizable(false);// cấm sửa kích thước cột trong bảng
            tcm.getColumn(i).setCellRenderer(cellRenderer);// căn lề phải đối với số

        }

        scrollPane.setViewportView(tblPrice);

        //add actionlistener
        btnConfirm.addActionListener(this);
        btnCancel.addActionListener(this);
        btnAdd.addActionListener(this);
        btnDelete.addActionListener(this);
    }

    //process button clicked
    @Override
    public void actionPerformed(ActionEvent ae) {
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        //confirm button clicked
        if (ae.getSource().equals(btnConfirm)) {
            if (tblPrice.isEditing()) {
                tblPrice.getCellEditor().stopCellEditing();
            }
            ArrayList<ElectricityPrice> ePrices = getDataFromTable();
            if (ePrices != null) {
                ePrices = arrangeTableData(ePrices);
                fillTable(ePrices);
                String result = checkConfigValid(ePrices);
                if (result == null) {
                    /**
                     * cầu hình chính xác
                     */
                    System.out.println("cấu hình chuẩn");
                    /**
                     * hiện thông báo chọn yes no
                     */
                    int choice = JOptionPane.showConfirmDialog(this, "Thay đổi cấu hình đã được chấp nhận. Bạn có muốn lưu cấu hình?", null, JOptionPane.YES_NO_OPTION);
                    if (choice == JOptionPane.YES_OPTION) {
                        //chọn YES -> update db
                        electricityPriceDAO.deleteAll();
                        electricityPriceDAO.addValues(ePrices);
                        //báo hiệu cho giao diện quản lý cấu hình
                        onDataChangeListener.onDataChange(ePrices);
                        // đóng cửa số
                        this.dispose();
                    } else {// chọn NO -> ko làm gì cả

                    }

                } else {
                    /**
                     * cấu hình ko chính xác báo dòng bị lỗi
                     */
                    JOptionPane.showMessageDialog(this, result, "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
        //cancel button clicked
        if (ae.getSource().equals(btnCancel)) {
            int choice = JOptionPane.showConfirmDialog(this, "Các chỉnh sửa sẽ không được lưu lại. Bạn có chắc muốn thoát?", null, JOptionPane.YES_NO_OPTION);
            if (choice == JOptionPane.YES_OPTION) {
                this.dispose();
            }
        }
        //add button clicked
        if (ae.getSource().equals(btnAdd)) {
            int index = tblPrice.getSelectedRow();
            if (index >= 0) {
                dtm.insertRow(index + 1, new Object[]{null, null, null, null});
            } else {
                dtm.addRow(new Object[]{null, null, null, null});
            }
            updateSTT(dtm);
        }
        //delete button clicked
        if (ae.getSource().equals(btnDelete)) {
            int index = tblPrice.getSelectedRow();
            if (index >= 0) {
                // hiển thị message xác nhận
                int choice = JOptionPane.showConfirmDialog(this, "Bạn có chắc chắn muốn xóa dòng " + (index + 1) + " ?", null, JOptionPane.YES_NO_OPTION);
                // nếu chọn yes
                if (choice == JOptionPane.YES_OPTION) {
                    dtm.removeRow(index);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Chọn 1 dòng để xóa.");
            }
            updateSTT(dtm);

        }
    }

    // đổ dữ liệu lên bảng
    private void fillTable(ArrayList<ElectricityPrice> ePrices) {
        NumberFormat formatter = new DecimalFormat("#0.000");
        ElectricityPrice ePrice;
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        dtm.setRowCount(0);
        for (int i = 0; i < ePrices.size(); i++) {
            ePrice = ePrices.get(i);
            Object[] object = new Object[4];
            object[0] = i + 1;
            object[1] = ePrice.getLowerBound();
            if (ePrice.getUpperBound() == -1) {
                object[2] = "--";
            } else {
                object[2] = ePrice.getUpperBound();
            }
            object[3] = formatter.format(ePrice.getPrice());
            dtm.addRow(object);
        }
    }

    //update stt column in the table
    private void updateSTT(DefaultTableModel dtm) {
        for (int i = 0; i < dtm.getRowCount(); i++) {
            dtm.setValueAt(i + 1, i, 0);
        }
    }

    //get input data from the table 
    //to make new set of configuration
    private ArrayList<ElectricityPrice> getDataFromTable() {
        ArrayList<ElectricityPrice> ePrices = new ArrayList<ElectricityPrice>();
        DefaultTableModel dtm = (DefaultTableModel) tblPrice.getModel();
        for (int row = 0; row < dtm.getRowCount(); row++) {
            String[] objects = new String[3];
            objects[0] = dtm.getValueAt(row, 1).toString();
            objects[1] = dtm.getValueAt(row, 2).toString();
            objects[2] = dtm.getValueAt(row, 3).toString();
            
            ElectricityPrice ePrice = checkInputData(objects, row,dtm.getRowCount());
            if (ePrice == null) {
                return null;
            } else {
                ePrices.add(ePrice);
            }
        }
        return ePrices;
    }

    // check input validation 
    public ElectricityPrice checkInputData(String[] objects, int row, int size) {
        ElectricityPrice ePrice = new ElectricityPrice();
        int lowerBound;
        int upperBound;
        double price;
        
        String strLowerBound = objects[0];
            //kiểm tra giới hạn dưới dòng 1
            if(row == 0 && !strLowerBound.equals("0")){
                JOptionPane.showMessageDialog(this,"Giới hạn dưới của cấu hình đầu tiên phải = 0",null, JOptionPane.ERROR_MESSAGE);
                return null;
            }
            // kiểm tra ô rỗng cột Từ
            if (strLowerBound.equals("")){
                JOptionPane.showMessageDialog(this, "Giới hạn dưới không được để trống ở dòng " + (row+1),null ,JOptionPane.ERROR_MESSAGE);
                return null;
            }
            String strUpperBound= objects[1];
            //kiểm tra giới hạn trên ở dòng cuối
            if(row == size-1 && !strUpperBound.equals("--")){
                JOptionPane.showMessageDialog(this,"Giới hạn trên của cấu hình cuối phải là --",null, JOptionPane.ERROR_MESSAGE);
                return null;
            }
            // kiểm tra ô rỗng cột Tới
            if (strUpperBound.equals("")){
                JOptionPane.showMessageDialog(this, "Giới hạn trên không được để trống ở dòng " + (row+1),null ,JOptionPane.ERROR_MESSAGE);
                return null;
            }
            String strPrice = objects[2];
            //kiểm tra ô rỗng cột Giá
            if (strPrice.equals("")){
                JOptionPane.showMessageDialog(this, "Giá không được để trống ở dòng " + (row+1),null ,JOptionPane.ERROR_MESSAGE);
                return null;
            }
        /**
         * check if inputs are numbers;
         */
        try {
            lowerBound = Integer.parseInt(objects[0]);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Giới hạn dưới phải là 1 số nguyên ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        try {
            if (objects[1].equals("--")) {
                upperBound = -1;
            } else {
                upperBound = Integer.parseInt(objects[1]);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Giới hạn trên phải là 1 số nguyên ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        try {
            price = Double.parseDouble(objects[2]);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Giá tiền phải là 1 số thực ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        /**
         * check for validation
         */
        if (lowerBound < 0) {
            JOptionPane.showMessageDialog(this, "Giới hạn dưới phải lớn hơn hoặc bằng 0 ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        if (upperBound <= 0 && !objects[1].equals("--")) {
            JOptionPane.showMessageDialog(this, "Giới hạn trên phải lớn hơn 0 ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        if (price <= 0) {
            JOptionPane.showMessageDialog(this, "Giá tiền phải lớn hơn 0 ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        if (lowerBound >= upperBound && !objects[1].equals("--")) {
            JOptionPane.showMessageDialog(this, "Giới hạn dưới phải nhỏ hơn giới hạn trên ở dòng " + (row + 1), "Cấu hình lỗi", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        ePrice.setLowerBound(lowerBound);
        ePrice.setUpperBound(upperBound);
        ePrice.setPrice(price);
        return ePrice;
    }

    //sort table
    private ArrayList<ElectricityPrice> arrangeTableData(ArrayList<ElectricityPrice> ePrices) {
        ElectricityPrice temp;
        ElectricityPrice ePriceI;
        ElectricityPrice ePriceJ;

        for (int i = 0; i < ePrices.size() - 1; i++) {
            ePriceI = ePrices.get(i);
            for (int j = i + 1; j < ePrices.size(); j++) {
                ePriceJ = ePrices.get(j);
                if (ePriceI.getLowerBound() > ePriceJ.getLowerBound() && ePriceI.getUpperBound() > ePriceJ.getUpperBound()) {
                    temp = ePriceI;
                    ePriceI = ePriceJ;
                    ePriceJ = temp;
                }
                ePrices.set(i, ePriceI);
                ePrices.set(j, ePriceJ);
            }
        }
        return ePrices;
    }

    //check config valid
    public String checkConfigValid(ArrayList<ElectricityPrice> ePrices) {
        //check for if the upperbound of 
        //the previous price is equal to lowerbound 
        //of the next price +1
        for (int i = 1; i < ePrices.size(); i++) {
            if (ePrices.get(i).getLowerBound() - 1 != ePrices.get(i - 1).getUpperBound()) {
                return "Giới hạn dưới của dòng " + (i + 1) + " phải hơn giới hạn trên của dòng " + i + " chính xác 1 đơn vị";//return error
            }
        }
        return null;//if no error 
    }

    // interface báo hiệu cấu hình đã được chỉnh sửa thành công có giao diện quản lý giá điện
    public interface OnDataChangeListener {

        public void onDataChange(ArrayList<ElectricityPrice> ePrices);
    }

}
