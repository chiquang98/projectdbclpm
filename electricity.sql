-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 20, 2020 lúc 07:36 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `electricity`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `account`
--

CREATE TABLE `account` (
  `id` int(10) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `account`
--

INSERT INTO `account` (`id`, `username`, `password`) VALUES
(1, 'babach', '123456'),
(2, 'thanhdung', '123456'),
(3, 'vanhieu', '123456'),
(4, 'chiquang', '123456');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address`
--

CREATE TABLE `address` (
  `id` int(10) NOT NULL,
  `addressLine` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `address`
--

INSERT INTO `address` (`id`, `addressLine`, `district`, `city`, `country`) VALUES
(1, '', 'Gia Lâm', 'Hà Nội', 'Việt Nam'),
(2, '', 'Thanh Xuân', 'Hà Nội', 'Việt Nam'),
(3, '', 'Thanh Trì', 'Hà Nội', 'Việt Nam'),
(4, '', 'Đông Anh', 'Hà Nội', 'Việt Nam');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `customerID` varchar(255) NOT NULL,
  `fullNameID` int(10) DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `addressID` int(10) DEFAULT NULL,
  `accountID` int(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`customerID`, `fullNameID`, `dateOfBirth`, `addressID`, `accountID`, `email`, `tel`) VALUES
('001098021334', 1, '1998-12-30', 1, 1, 'ngbabach@gmail.com', '0333223535'),
('001188015634', 2, '1998-04-04', 2, 2, 'dungnt441998@gmail.com', '0366189608'),
('013612379', 4, '1998-04-01', 4, 4, 'chiquang1498@gmail.com', '0975312798'),
('184288789', 3, '1998-07-21', 3, 3, 'vanhieunguyen221@gmail.com', '0336838142');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `electricitybill`
--

CREATE TABLE `electricitybill` (
  `billID` int(10) NOT NULL,
  `electricityUseID` int(10) DEFAULT NULL,
  `payDate` date DEFAULT NULL,
  `totalPaid` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `electricitybill`
--

INSERT INTO `electricitybill` (`billID`, `electricityUseID`, `payDate`, `totalPaid`) VALUES
(69, 37, '2019-06-19', 210.88),
(70, 38, '2019-06-20', 481.05),
(71, 39, '2019-06-19', 243.1),
(72, 40, '2019-06-17', 741.79),
(73, 41, '2019-07-08', 239.08),
(74, 42, '2019-07-09', 377.07),
(75, 43, '2019-07-14', 279.36),
(76, 44, '2019-07-07', 379.61),
(77, 45, '2019-08-19', 142.86),
(78, 46, '2019-08-18', 97.77),
(79, 47, '2019-08-08', 115.11),
(80, 48, '2019-08-20', 102.97),
(81, 49, '2019-09-16', 273.31),
(82, 50, '2019-09-17', 313.59),
(83, 51, '2019-09-11', 178.66),
(84, 52, '2019-09-20', 363.94),
(85, 53, '2019-10-19', 382.14),
(86, 54, '2019-10-05', 311.58),
(87, 55, '2019-10-15', 186.71),
(88, 56, '2019-10-15', 161.93),
(89, 57, '2019-11-15', 501.34),
(90, 58, '2019-11-13', 128.98),
(91, 59, '2019-11-19', 329.71),
(92, 60, '2019-11-13', 226.99),
(93, 61, '2019-12-01', 1020.23),
(94, 62, '2019-12-08', 605.31),
(95, 63, '2019-12-06', 369.99),
(96, 64, '2019-12-15', 130.72),
(97, 65, '2020-01-08', 503.87),
(98, 66, '2020-01-04', 394.82),
(99, 67, '2020-01-19', 321.65),
(100, 68, '2020-01-17', 357.9),
(101, 69, '2020-02-10', 151.53),
(102, 71, '2020-02-09', 546.98);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `electricityprice`
--

CREATE TABLE `electricityprice` (
  `id` int(10) NOT NULL,
  `lowerBound` int(10) DEFAULT NULL,
  `upperBound` int(10) DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `electricityprice`
--

INSERT INTO `electricityprice` (`id`, `lowerBound`, `upperBound`, `price`) VALUES
(1, 0, 50, 1.678),
(2, 51, 100, 1.734),
(3, 101, 200, 2.014),
(4, 201, 300, 2.536),
(5, 301, 400, 2.834),
(6, 401, NULL, 2.927);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `electricityuse`
--

CREATE TABLE `electricityuse` (
  `useID` int(10) NOT NULL,
  `customerID` varchar(255) DEFAULT NULL,
  `useDate` date DEFAULT NULL,
  `lastNumber` int(10) DEFAULT NULL,
  `currentNumber` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `electricityuse`
--

INSERT INTO `electricityuse` (`useID`, `customerID`, `useDate`, `lastNumber`, `currentNumber`) VALUES
(37, '001098021334', '2019-05-01', 0, 120),
(38, '001188015634', '2019-05-01', 0, 243),
(39, '184288789', '2019-05-01', 0, 136),
(40, '013612379', '2019-05-01', 0, 341),
(41, '001098021334', '2019-06-01', 120, 254),
(42, '001188015634', '2019-06-01', 243, 445),
(43, '184288789', '2019-06-01', 136, 290),
(44, '013612379', '2019-06-01', 341, 544),
(45, '001098021334', '2019-07-01', 254, 338),
(46, '001188015634', '2019-07-01', 445, 503),
(47, '184288789', '2019-07-01', 290, 358),
(48, '013612379', '2019-07-01', 544, 605),
(49, '001098021334', '2019-08-01', 338, 489),
(50, '001188015634', '2019-08-01', 503, 674),
(51, '184288789', '2019-08-01', 358, 462),
(52, '013612379', '2019-08-01', 605, 801),
(53, '001098021334', '2019-09-01', 489, 693),
(54, '001188015634', '2019-09-01', 674, 844),
(55, '184288789', '2019-09-01', 462, 570),
(56, '013612379', '2019-09-01', 801, 896),
(57, '001098021334', '2019-10-01', 693, 944),
(58, '001188015634', '2019-10-01', 844, 920),
(59, '184288789', '2019-10-01', 570, 749),
(60, '013612379', '2019-10-01', 896, 1024),
(61, '001098021334', '2019-11-01', 944, 1382),
(62, '001188015634', '2019-11-01', 920, 1212),
(63, '184288789', '2019-11-01', 749, 948),
(64, '013612379', '2019-11-01', 1024, 1101),
(65, '001098021334', '2019-12-01', 1382, 1634),
(66, '001188015634', '2019-12-01', 1212, 1421),
(67, '184288789', '2019-12-01', 948, 1123),
(68, '013612379', '2019-12-01', 1101, 1294),
(69, '001098021334', '2020-01-01', 1634, 1723),
(70, '001188015634', '2020-01-01', 1421, 1584),
(71, '184288789', '2020-01-01', 1123, 1392),
(72, '013612379', '2020-01-01', 1294, 1492);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `fullname`
--

CREATE TABLE `fullname` (
  `id` int(10) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `fullname`
--

INSERT INTO `fullname` (`id`, `firstName`, `middleName`, `lastName`) VALUES
(1, 'Bách', 'Bá', 'Nguyễn'),
(2, 'Dung', 'Thanh', 'Nguyễn'),
(3, 'Hiệu', 'Văn', 'Nguyễn'),
(4, 'Quang', 'Chí', 'Trần');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`),
  ADD KEY `fullNameID` (`fullNameID`),
  ADD KEY `addressID` (`addressID`),
  ADD KEY `accountID` (`accountID`);

--
-- Chỉ mục cho bảng `electricitybill`
--
ALTER TABLE `electricitybill`
  ADD PRIMARY KEY (`billID`),
  ADD KEY `electricityUseID` (`electricityUseID`);

--
-- Chỉ mục cho bảng `electricityprice`
--
ALTER TABLE `electricityprice`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `electricityuse`
--
ALTER TABLE `electricityuse`
  ADD PRIMARY KEY (`useID`),
  ADD KEY `customerID` (`customerID`);

--
-- Chỉ mục cho bảng `fullname`
--
ALTER TABLE `fullname`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `account`
--
ALTER TABLE `account`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `address`
--
ALTER TABLE `address`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `electricitybill`
--
ALTER TABLE `electricitybill`
  MODIFY `billID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT cho bảng `electricityprice`
--
ALTER TABLE `electricityprice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `electricityuse`
--
ALTER TABLE `electricityuse`
  MODIFY `useID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT cho bảng `fullname`
--
ALTER TABLE `fullname`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`fullNameID`) REFERENCES `fullname` (`id`),
  ADD CONSTRAINT `customer_ibfk_2` FOREIGN KEY (`addressID`) REFERENCES `address` (`id`),
  ADD CONSTRAINT `customer_ibfk_3` FOREIGN KEY (`accountID`) REFERENCES `account` (`id`);

--
-- Các ràng buộc cho bảng `electricitybill`
--
ALTER TABLE `electricitybill`
  ADD CONSTRAINT `electricitybill_ibfk_1` FOREIGN KEY (`electricityUseID`) REFERENCES `electricityuse` (`useID`);

--
-- Các ràng buộc cho bảng `electricityuse`
--
ALTER TABLE `electricityuse`
  ADD CONSTRAINT `electricityuse_ibfk_1` FOREIGN KEY (`customerID`) REFERENCES `customer` (`customerID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
